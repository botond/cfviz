## Build Instructions and Requirements

The visualization component ('cfviz-frontend') is built in Rust, and currently 
requires cargo (or at least rustc) to build from source. Ensure that you have 
[installed](https://www.rust-lang.org/en-US/install.html) Rust, and after cloning the 
[this repository](https://bitbucket.org/botond/cfviz/), run

    make release

in the parent directory to build. 

## Usage

To use, run:

    source <srcdir>/backend/cfviz.py

inside a gdb command prompt (where `<srcdir>` is the root directory of the
cfviz checkout. Then run `cfviz` at any time to visualize the control
flow of the current function. 

## Usage Notes

This plugin supports both vanilla gdb and rr. However, note that with
vanilla gdb, the `cfviz` command will only visualize the remainder of the function's
execution, while with rr it will visualize the entire function execution
no matter what point in the function it's invoked from.

### Issues with Color Display

Currently, the output is colorized via ANSI CSI sequences, with 24-bit
("TrueColor") color support enabled by default. Most modern terminal emulators
[should handle this correctly](https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit),
however some standard terminal emulators (e.g. MacOS terminal.app) do not 
seem to be up to par.

The visualization component has a number of different color options
implemented. If you find that the 24-bit colors are not being displayed 
correctly on your terminal, instead of running `cfviz` you can run:

    cfviz --color=256

to limit output to 8-bit (256) ANSI colors, or 

    cfviz --raw

to produce output without any ANSI escape codes at all (should be compatible even
with non-ANSI terminals). 

The visualization component can also display a color test which you can use
to see which color options your terminal can support. To run this test, make
sure you are in the root directory of the cfviz checkout, and then run:

    cd frontend/cmd && cargo run -- --colortest
