# Data Format

This tool consists of two components:

  1. A component that integrates with a debugger, and produces
     data representing a particular execution of a function.

  2. A component that visualizes the control flow within the
     function for that execution.

The first component will provide the second with JSON-formatted
data. The format of that data will be as follows:

```
{
  "function": "<code>",
  "execution": [
    <list of line numbers>
  ],
  "first_line_number": <line number>
}
```

Here, "\<code\>" is the function's source code, with newlines and 
other spacing inside it intended to be preserved.

"\<list of line numbers\>" is a list of line numbers, each one
interpreted as a 0-based index into the lines of code contained
in "\<code\>". The list has one element for every step in the
function's execution (where a "step" is defined as execution of
a complete line of code); note that this may be more or less than
the total number of lines in the function, due to conditional
control flow and loops. The value of the Nth element in the list
is the line number of the line of code executed during the Nth
step of the function's execution.

"\<line number\>" is the real line number of the first line of
the function in the original source file. This allows the 
visualizer component to display real line numbers if desired.

To illustrate with an example, consider the following function:

```
void foo(int times, bool cond) {
  int index = 0;
  while (index < times) {
    std::cout << ++index << "\n";
  }
  if (cond) {
    std::cout << "conditional branch\n"; 
  }
}
```

For the execution of `foo(3, false)`, the data would be:


```
{
  "function": "...",  # the code as written above
  "execution": [
    1, 2, 3, 4, 2, 3, 4, 2, 3, 4, 2, 5, 8
  ]
}
```

This corresponds to the sequence of lines a typical debugger 
would highlight if you stepped through the entire function with
the debugger's "next" command.
