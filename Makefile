all: cmd-frontend

# Command-line frontend

lnrm:
	rm -f ./frontend/cfviz-frontend

## Debug

cmd-frontend: build-debug lnrm lndebug

build-debug:
	cd frontend/cmd && cargo build

lndebug:
	ln -s ./cmd/target/debug/cfviz-frontend ./frontend/

## Release

release: build-release lnrm lnrelease

build-release:
	cd frontend/cmd && cargo build --release

lnrelease:
	ln -s ./cmd/target/release/cfviz-frontend ./frontend/


# Web frontend (not currently enabled or hooked up)

web-frontend: css

css:
	@stylus frontend/web/styles/style.styl

# Checks

check: lint test

lint:
	cd frontend/cmd && cargo +nightly clippy

test:
	mkdir -p test/bin
	mkdir -p test/actual
	cd test && ./sanity-test.sh

# Format

fmt:
	cd frontend/cmd && cargo +nightly fmt

# Cleanup

clean:
	rm -f frontend/web/styles/style.css
	rm -f test/test-program
	rm -f test/cfviz-output.actual

.PHONY: all build-debug build-release check clean cmd-frontend css fmt lint lndebug lnrelease lnrm test release web-frontend
