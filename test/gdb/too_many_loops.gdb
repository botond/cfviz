# source the extension
source ../backend/cfviz.py

# Break on line 1, at the beginning of the function 'too_many_loops'
break 2

# Run the program until the breakpoint, swallowing its output
run > /dev/null

# Invoke "cfviz"
cfviz
