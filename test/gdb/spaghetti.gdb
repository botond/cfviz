# source the extension
source ../backend/cfviz.py

# Break on line 4, at the beginning of the function 'spaghetti'
break 4

# Run the program until the breakpoint, swallowing its output
run > /dev/null

# Invoke "cfviz"
cfviz
