# source the extension
source ../backend/cfviz.py

# Break on line 1, at the beginning of the function 'empty'
break 1

# Run the program until the breakpoint, swallowing its output
run > /dev/null

# Invoke "cfviz"
cfviz
