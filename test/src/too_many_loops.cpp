int too_many_loops(int power)
{
    int count = 0;
    for (int i = 0; i < power; ++i) {
        for (int j = 0; j < power; ++j) {
            for (int k = 0; k < power; ++k) {
                count++;
            }
        }
    }
    return count;
}

int main()
{
    too_many_loops(5);
    return 0;
}
