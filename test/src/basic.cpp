#include <iostream>

void foo(int times, bool cond) {
  int index = 0;
  while (index < times) {
    std::cout << ++index << "\n";
  }
  if (cond) {
    std::cout << "conditional branch\n"; 
  }
}

int main() {
  foo(3, false);
}
