#include <iostream>

void spaghetti(int start) {
    const int times = 5;
    int branch = start;
    while (branch < times) {
        switch (branch) {
            case 0:
                std::cout << branch << std::endl;
                branch = 3;
                break;
            case 1:
                std::cout << branch << std::endl;
                branch = 4;
                break;
            case 2:
                std::cout << branch << std::endl;
                branch = 1;
                break;
            case 3:
                std::cout << branch << std::endl;
                branch = 2;
                break;
            case 4:
                std::cout << branch << std::endl;
                ++branch;
                break;
            default:
                ++branch;
                break;
        }
    }
}

int main() {
  spaghetti(0);
}

