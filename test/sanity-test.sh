#!/bin/bash

# Files for testing Just add source files to the array here
# and the rest will be handled automatically
sources=(basic.cpp spaghetti.cpp empty.cpp too_many_loops.cpp)

# Names of compiled binaries (just removes '.cpp' from the source file names)
compiles=(${sources[@]/.cpp/})

# Names of .gdb command files (just replaces '.cpp' with '.gdb')
gdbs=(${sources[@]/.cpp/.gdb})


# directories for above files
SRCDIR="src/"      # .cpp source files for testing
BINDIR="bin/"      # compiled binary files
GDBDIR="gdb/"      # .gdb command files
ACTDIR="actual/"   # actual (current) cfviz output from most recent test run
EXPDIR="expected/" # expected (correct) output (should be version-controlled)

# echo "${gdbs[@]}"

# Compile the example programs.
for i in ${!sources[*]}
do
    g++ -g -o $BINDIR${compiles[i]} $SRCDIR${sources[i]}
done

# Run the programs under the debugger.
#   * |--command gdb-commands.gdb| tells gdb to execute the commands
#     listed on |gdb-commands.gdb| on startup.
#   * |--batch-silent| tells gdb to exit after executing the startup
#     commands, and to suppress output (of its own; the cfviz frontend's
#     output is still displayed).
# The remaining output is redirected to the file |cfviz-output.actual|.

actuals=(${sources[@]/.cpp/_out.actual})

for i in ${!sources[*]}
do
    gdb --batch-silent --command $GDBDIR${gdbs[i]} $BINDIR${compiles[i]} > $ACTDIR${actuals[i]}
done


# Some setup for producing colored output later.
# Taken from https://stackoverflow.com/questions/4717746/how-to-display-build-errors-in-red.
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
NORMAL=$(tput sgr0)


# Diff the actual output against the expected output.
# Diff will return a success (0) exit code if the files are the same,
# so we can use that to inform the user whether the test passed.
expecteds=(${actuals[@]/actual/expected})

SUCCESS=0
ERR=0
printf "\n\n%sRunning tests...%s\n" ${BLUE} ${NORMAL}
# echo "\n\n${BLUE}Running tests...${NORMAL}\n"
for i in ${!sources[*]}
do
    echo "${NORMAL}Testing ${sources[i]}:${NORMAL}"
    if diff $ACTDIR${actuals[i]} $EXPDIR${expecteds[i]}
    then
        echo "      ......${GREEN}Test passed!${NORMAL}"
        echo
        ((SUCCESS++))
    else
        echo "      ......${RED}Test failed.${NORMAL}"
        echo
        ((ERR++))
    fi
done

echo "${BLUE}Tests complete.${NORMAL}"
if [ "$ERR" -eq "0" ]
then
    echo "      ......${GREEN}SUCCESS${NORMAL}. All ${GREEN}$SUCCESS${NORMAL} tests passed."
    echo
    exit 0
else
    echo "      ......${GREEN}$SUCCESS${NORMAL} tests ${GREEN}PASSED${NORMAL}. ${RED}$ERR${NORMAL} tests ${RED}FAILED${NORMAL}."
    echo
    exit 1
fi

echo
exit 0
