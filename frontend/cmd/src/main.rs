extern crate clap;

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

extern crate termion;

use std::fs::File;
use std::collections::VecDeque;

use termion::color;
use termion::color::{AnsiValue, Color, Fg, Rgb};

use clap::ArgMatches;


const PALETTE_SIZE: usize = 10;

// this can be optimized to be less silly later, for now this is to allow
// easy implementation of the `--no-color` command-line option
const HEATMAP_NOCOL: [Fg<AnsiValue>; PALETTE_SIZE] = [
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
    Fg(AnsiValue(15)),
];

const HEATMAP_TRUECOL: [Fg<Rgb>; PALETTE_SIZE] = [
    Fg(Rgb(240, 105, 99)), // reddest
    Fg(Rgb(235, 100, 116)),
    Fg(Rgb(226, 98, 132)),
    Fg(Rgb(212, 100, 146)),
    Fg(Rgb(194, 103, 159)),
    Fg(Rgb(172, 108, 168)),
    Fg(Rgb(147, 113, 174)),
    Fg(Rgb(119, 118, 176)),
    Fg(Rgb(88, 121, 173)),
    Fg(Rgb(52, 123, 167)), // bluest
];

const HEATMAP256: [Fg<AnsiValue>; PALETTE_SIZE] = [
    Fg(AnsiValue(197)), // bright red
    Fg(AnsiValue(198)),
    Fg(AnsiValue(206)),
    Fg(AnsiValue(135)),
    Fg(AnsiValue(99)),
    Fg(AnsiValue(105)),
    Fg(AnsiValue(111)),
    Fg(AnsiValue(75)),
    Fg(AnsiValue(39)),
    Fg(AnsiValue(33)), // bright blue
];

// #0656AB,#2B59A5,#3D5B9F,#4A5E98,#556191,#5F648A,#676781,#6E6A79,#756E6F,#7B7165
// http://tristen.ca/hcl-picker/#/clh/10/250/0656AB/7B7165
const HEATMAP_TRUE_MONO_DARK_BLUE: [Fg<Rgb>; PALETTE_SIZE] = [
    Fg(Rgb(123, 113, 101)), // grayest
    Fg(Rgb(117, 110, 111)),
    Fg(Rgb(110, 106, 121)),
    Fg(Rgb(103, 103, 129)),
    Fg(Rgb(95, 100, 138)),
    Fg(Rgb(85, 97, 145)),
    Fg(Rgb(74, 94, 152)),
    Fg(Rgb(61, 91, 159)),
    Fg(Rgb(43, 89, 165)),
    Fg(Rgb(6, 86, 171)), // bluest
];

#[allow(dead_code)]
const HEATMAP256_MONO_DARK_BLUE: [Fg<AnsiValue>; PALETTE_SIZE] = [
    Fg(AnsiValue(242)), // grayest
    Fg(AnsiValue(240)),
    Fg(AnsiValue(59)),
    Fg(AnsiValue(67)),
    Fg(AnsiValue(25)),
    Fg(AnsiValue(26)),
    Fg(AnsiValue(21)),
    Fg(AnsiValue(20)),
    Fg(AnsiValue(19)),
    Fg(AnsiValue(27)), // brightest blue
];

// a "middle grey" that should probably have acceptable
// contrast for most light/dark terminal schemes
const GREY_TRUE: Fg<Rgb> = Fg(Rgb(119, 119, 119));

// assume user has configured their terminal scheme to "Bright Black" is visible
const GREY256: Fg<AnsiValue> = Fg(AnsiValue(8));

// as above, this is a bit of unnecessary data to make implementing `--no-color`
// more straightforward
const GREY_NOCOL: Fg<AnsiValue> = Fg(AnsiValue(8));

// strictly for importing via serde_json
#[derive(Deserialize)]
struct BackendData {
    function: String,
    execution: VecDeque<usize>,
    first_line_number: usize,
}

struct Line {
    source: String,
    number: usize,
    exec_count: usize,
    exec_density: usize,
}

impl Line {
    fn new(s: &str, offset: usize, first_line_num: &usize) -> Line {
        Line {
            source: s.to_owned(),
            number: offset + first_line_num,
            exec_count: 0,
            exec_density: 0,
        }
    }
}

struct Heatmap {
    lines: Vec<Line>,
    line_col_width: usize,
    exec_col_width: usize,
}

impl Heatmap {
    fn from_raw(source: &BackendData) -> Self {
        // build lines without execution counts or density
        let mut lines: Vec<Line> = source
            .function
            .split('\n')
            .enumerate()
            .map(|(i, s)| Line::new(s, i, &source.first_line_number))
            .collect();

        // tally number of times each line executed, saving the largest execution count
        let mut max_exec_count: usize = 0;
        for line_num in &source.execution {
            if *line_num >= lines.len() {
                eprintln!(
                    "Invalid execution data: execution data points to line outside of function."
                );
                std::process::exit(1);
            }
            lines[*line_num].exec_count += 1;
            if lines[*line_num].exec_count > max_exec_count {
                max_exec_count = lines[*line_num].exec_count;
            }
        }

        // make so heatmap colours correspond to how often each line is executed
        // relative to the most executed line
        for line in &mut lines {
            line.exec_density = (PALETTE_SIZE - 1) * line.exec_count / max_exec_count;
        }

        // remove any lines generated from newlines after function closing brace
        while lines.last().unwrap().exec_count == 0 {
            lines.pop();
        }

        let max_line = lines.last().unwrap().number;
        let line_col_width = format!("{}", max_line).len().max("Line".len());
        let exec_col_width = format!("{}", max_exec_count).len().max("Count".len());

        Heatmap {
            lines,
            line_col_width,
            exec_col_width,
        }
    }
    fn from_file(path: &str) -> std::io::Result<Self> {
        let backend_out = File::open(path)?;
        let mut unprocessed: BackendData = serde_json::from_reader(backend_out)?;
        if unprocessed.execution.is_empty() {
            eprintln!("Error: No execution data.");
            std::process::exit(1);
        }
        if unprocessed.execution[0] != 0 {
            unprocessed.execution.push_front(0);
        };
        Ok(Heatmap::from_raw(&unprocessed))
    }

    // sizing has to be done **before** the execution count is coloured, because
    // the format! macro will read the escape codes as contributing to the
    // string length, and will thus incorrectly pad the results
    fn resized_columns(&self, config: &Config) -> Vec<(String, String, String)> {
        let ellipsis = String::from("[...]");
        // The '1' is from "|".len() and the '3' is from " | ".len() later in Line::format
        let all_columns_width = self.line_col_width + self.exec_col_width + 1 + 3;
        let last_column_width = config.width - all_columns_width;

        let mut line_columns = Vec::with_capacity(self.lines.len());
        for line in &self.lines {
            let resized_line_number =
                format!("{:^width$}", line.number, width = self.line_col_width);
            let resized_exec_count =
                format!("{:>width$}", line.exec_count, width = self.exec_col_width);
            let resized_source;
            if line.source.len() > last_column_width {
                resized_source = format!(
                    "{}{}",
                    &line.source[0..(last_column_width - ellipsis.len())],
                    &ellipsis,
                );
            } else {
                resized_source = format!("{}", &line.source);
            };
            line_columns.push((resized_line_number, resized_exec_count, resized_source));
        }
        line_columns
    }
    fn color_columns(&self, config: &Config) -> Vec<(String, String, String)> {
        let mut color_columns = Vec::with_capacity(self.lines.len());
        let resized_columns = self.resized_columns(config);
        for (i, columned_line) in resized_columns.iter().enumerate() {
            let colored_exec;
            let colored_src;
            if let Some(ref depth) = config.color {
                // 'base' and 'zero' cases should eventually get proper proper pallettes
                // and/or separate function calls that don't insert escape codes at all
                match *depth {
                    ColorDepth::TrueColor => {
                        colored_exec = format!(
                            "{color}{exec}{reset}",
                            color = HEATMAP_TRUECOL[self.lines[i].exec_density],
                            exec = &columned_line.1,
                            reset = Fg(color::Reset),
                        );
                        if self.lines[i].exec_count == 0 {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = GREY_TRUE,
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        } else {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = HEATMAP_TRUECOL[self.lines[i].exec_density],
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        }
                    }
                    ColorDepth::ANSI256 => {
                        colored_exec = format!(
                            "{color}{exec}{reset}",
                            color = HEATMAP256[self.lines[i].exec_density],
                            exec = &columned_line.1,
                            reset = Fg(color::Reset),
                        );
                        if self.lines[i].exec_count == 0 {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = GREY256,
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        } else {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = HEATMAP256[self.lines[i].exec_density],
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        }
                    }
                    ColorDepth::Base => {
                        colored_exec = format!(
                            "{color}{exec}{reset}",
                            color = HEATMAP_NOCOL[self.lines[i].exec_density],
                            exec = &columned_line.1,
                            reset = Fg(color::Reset),
                        );
                        if self.lines[i].exec_count == 0 {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = GREY_NOCOL,
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        } else {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = HEATMAP_NOCOL[self.lines[i].exec_density],
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        }
                    }
                    ColorDepth::Zero => {
                        colored_exec = format!(
                            "{color}{exec}{reset}",
                            color = HEATMAP_NOCOL[self.lines[i].exec_density],
                            exec = &columned_line.1,
                            reset = Fg(color::Reset),
                        );
                        if self.lines[i].exec_count == 0 {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = GREY_NOCOL,
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        } else {
                            colored_src = format!(
                                "{color}{src}{reset}",
                                color = HEATMAP_NOCOL[self.lines[i].exec_density],
                                src = &columned_line.2,
                                reset = Fg(color::Reset),
                            );
                        }
                    }
                }
            } else {
                colored_exec = format!(
                    "{color}{exec}{reset}",
                    color = HEATMAP_TRUECOL[self.lines[i].exec_density],
                    exec = &columned_line.1,
                    reset = Fg(color::Reset),
                );
                if self.lines[i].exec_count == 0 {
                    colored_src = format!(
                        "{color}{src}{reset}",
                        color = GREY_TRUE,
                        src = &columned_line.2,
                        reset = Fg(color::Reset),
                    );
                } else {
                    colored_src = format!(
                        "{color}{src}{reset}",
                        color = HEATMAP_TRUECOL[self.lines[i].exec_density],
                        src = &columned_line.2,
                        reset = Fg(color::Reset),
                    );
                }
            }
            color_columns.push((columned_line.0.clone(), colored_exec, colored_src));
        }
        color_columns
    }
    fn printout(&self, config: &Config) {
        let columns = if !config.mode_is_raw {
            self.color_columns(config)
        } else {
            self.resized_columns(config)
        };
        println!("{}", self.build_header(config.width));
        for column in columns {
            println!("{}|{} | {}", column.0, column.1, column.2);
        }
        println!("{}", "=".repeat(config.width));
    }
    fn build_header(&self, width: usize) -> String {
        let boundary = "=".repeat(width);

        let line1_start = format!(
            "{:width1$}|{:width2$} |",
            "Line",
            "Exec.",
            width1 = self.line_col_width,
            width2 = self.exec_col_width,
        );
        let title = "Source Code";
        let pad_title_start = " ".repeat((width - title.len() - line1_start.len()) / 2);
        let pad_title_end = " ".repeat(((width - title.len() - line1_start.len()) / 2) + 1);
        let line1 = format!(
            "{}{}{}{}",
            line1_start,
            pad_title_start,
            title,
            pad_title_end,
        );

        let line2_start = format!(
            "{:width1$}|{:width2$} |",
            "  # ",
            "Count",
            width1 = self.line_col_width,
            width2 = self.exec_col_width,
        );
        let line2_end = " ".repeat(width - line2_start.len());
        let line2 = format!("{}{}", line2_start, line2_end);

        format!("{}\n{}\n{}\n{}", boundary, line1, line2, boundary,)
    }
}

fn test_heatmap_colors<C, G>(
    palette: &[Fg<C>; PALETTE_SIZE],
    grey: &Fg<G>,
    color_text: &str,
    command: &str,
) where
    C: Color + 'static,
    G: Color + 'static,
{
    println!(
        "{}Running test for heatmap option: (`{}`)\n",
        Fg(AnsiValue(2)),
        command
    );
    for color in palette {
        println!("{}{}{}", color, color_text, Fg(color::Reset));
    }
    println!("{}## UNEXECUTED ##{}", grey, Fg(color::Reset));
}

fn color_test() {
    test_heatmap_colors(&HEATMAP_NOCOL, &GREY_NOCOL, "NO COLOR...", "--color=none");
    test_heatmap_colors(&HEATMAP256, &GREY256, "256COLOR :(", "--color=256");
//    test_heatmap_colors(
//        &HEATMAP256_MONO_DARK_BLUE,
//        &GREY256,
//        "265MONO BLUE",
//        "--mono [WIP]",
//    );
    test_heatmap_colors(&HEATMAP_TRUECOL, &GREY_TRUE, "TRUECOLOR!", "--color=true");
    test_heatmap_colors(
        &HEATMAP_TRUE_MONO_DARK_BLUE,
        &GREY_TRUE,
        "TRUEMONO DARK BLUE",
        "--color=true --mono",
    );
}

fn demo(config: &Config) -> std::io::Result<()> {
    let testfiles = [
        "../../json/spaghetti.json",
        "../../json/ridiculous.json",
        "../../json/simple.json",
    ];
    match config.color {
        None if config.mode_is_raw => {}
        Some(ColorDepth::ANSI256) => {
            test_heatmap_colors(&HEATMAP256, &GREY256, "256COLOR :(", "--color=256");
        }
        None | Some(ColorDepth::TrueColor) => {
            test_heatmap_colors(&HEATMAP_TRUECOL, &GREY_TRUE, "TRUECOLOR!", "--color=true");
        }
        Some(ColorDepth::Base) | Some(ColorDepth::Zero) => {
            test_heatmap_colors(&HEATMAP_NOCOL, &GREY_NOCOL, "NO COLOR...", "--color=none");
        }
    }
    for path in &testfiles {
        print_heatmap_for_file(path, config)?;
    }
    Ok(())
}


// convenience helper functions
fn print_heatmap(config: &Config) -> std::io::Result<()> {
    let heatmap = Heatmap::from_file(&config.input)?;
    heatmap.printout(config);
    Ok(())
}

fn print_heatmap_for_file(path: &str, config: &Config) -> std::io::Result<()> {
    let heatmap = Heatmap::from_file(path)?;
    heatmap.printout(config);
    Ok(())
}

// make sure to print all error info, without debug formatting
fn log_error(mut err: &std::error::Error) {
    eprintln!("error: {}", err);
    while let Some(cause) = err.cause() {
        eprintln!("caused by: {}", cause);
        err = cause;
    }
}


// CLI Argument Handling
// some of this is a bit excessive, but might prove useful for later extensions
// e.g. allowing configuration via a .cfvizrc of some sort
enum ColorDepth {
    TrueColor,
    ANSI256,
    Base,
    Zero,
}

impl ColorDepth {
    fn from_args(matches: &ArgMatches) -> Option<ColorDepth> {
        if let Some(s) = matches.value_of("COLOR") {
            match s {
                "true" => Some(ColorDepth::TrueColor),
                "256" => Some(ColorDepth::ANSI256),
                "base" => Some(ColorDepth::Base),
                "none" => Some(ColorDepth::Zero),
                _ => unreachable!("clap failed to detect invalid --color arg"),
            }
        } else {
            None
        }
    }
}

// annotate will eventually be implemented
#[allow(dead_code)]
struct Config {
    input: String,
    color: Option<ColorDepth>,
    width: usize,
    annotation: Option<String>,
    mode_is_raw: bool,
    mode_is_colortest: bool,
    mode_is_demo: bool,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            input: String::new(),
            color: Some(ColorDepth::ANSI256),
            width: 80,
            annotation: None,
            mode_is_raw: false,
            mode_is_colortest: false,
            mode_is_demo: false,
        }
    }
}

impl Config {
    fn from_args(matches: &ArgMatches) -> Self {
        Config {
            // clap requires an input arg, is `None` only with special flags
            input: matches.value_of("INPUT").unwrap_or("").to_owned(),
            color: ColorDepth::from_args(matches),
            width: width_from_args(matches),
            annotation: if matches.occurrences_of("ANNOTATE") == 0 {
                None
            } else {
                matches.value_of("ANNOTATE").map(|s| s.to_owned())
            },
            mode_is_raw: matches.is_present("RAW"),
            mode_is_colortest: matches.is_present("COLORTEST"),
            mode_is_demo: matches.is_present("DEMO"),
        }
    }
}

fn setup_clap_interface() -> ArgMatches<'static> {
    use clap::*;
    App::new("cfviz (frontend)")
        .author("Derek Berger: dmberger.dev@gmail.com")
        .about("Visualize function control flow in gdb")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file")
                .required_unless_one(&["COLORTEST", "DEMO"]),
        )
        .arg(
            Arg::with_name("COLOR")
                .short("c")
                .long("color")
                .value_name("COLORDEPTH")
                .takes_value(true)
                .possible_values(&["true", "256", "base", "none"])
                .min_values(1)
                .help("Choose color depth for display")
                .long_help(
                    "Choose color depth for display. Available color options are:
    true -- (default) use TrueColor (24-bit) colors
    256  -- use only ANSI 256 (8-bit) colors
    base -- restrict to base 16 (3/4-bit RGBI) colors
    none -- do not use any colors
 ",
                )
                .display_order(1)
                .hide_possible_values(true),
        )
        .arg(
            Arg::with_name("WIDTH")
                .short("w")
                .long("width")
                .value_name("WIDTH")
                .takes_value(true)
                .number_of_values(1)
                .help("Set width of output")
                .long_help(
                    "Sets the width (number of characters) of the output.
Minimum allowable width is 30 characters. When this
option is not set, or is invalid, cfviz attempts to
determine terminal width, and if this fails (e.g.
output is not a TTY, or does not otherwise support
querying of the width) defaults to 80 characters.
 ",
                )
                .display_order(2),
        )
        .arg(
            Arg::with_name("ANNOTATE")
                .short("a")
                .long("annotate")
                .value_name("ANNOTATE")
                .takes_value(true)
                .default_value("# !EXEC #")
                .help("Annotate unexecuted lines")
                .long_help(
                    "
###############################
#   Currently unimplemented   #
###############################

Display unexecuted lines with specified characters
at the beginning. E.g. using --annotate=##DEAD## will
result in annotations like:

    ##DEAD##  // this line just had a comment
    const x = 42; // this line was executed

Likely only useful when using `none` or `base` --color
options. If no custom characters are specified with
this option, the default annotation is `# !EXEC #`.
",
                )
                .display_order(3)
                .hide_default_value(true)
                .hide_possible_values(true),
        )
        .arg(
            Arg::with_name("RAW")
                .long("raw")
                .conflicts_with_all(&["COLOR", "ANNOTATE", "COLORTEST"])
                .help("Output without any formatting"),
        )
        .arg(
            Arg::with_name("COLORTEST")
                .long("colortest")
                .conflicts_with_all(&["INPUT", "COLOR", "WIDTH", "ANNOTATE", "DEMO"])
                .help("Show how available color palettes display"),
        )
        .arg(
            Arg::with_name("DEMO")
                .long("demo")
                .conflicts_with_all(&["INPUT", "ANNOTATE", "COLORTEST"])
                .help("Demonstrate output for current configuration on sample code"),
        )
        .get_matches()
}

fn width_from_args(matches: &ArgMatches) -> usize {
    let mut width: usize = 80;
    if let Some(arg) = matches.value_of("WIDTH") {
        width = arg.parse().unwrap();
        if width < 30 {
            eprintln!("User-specified width too small. Defaulting to 30 char width.");
            width = 80;
        }
    } else {
        let computed_size = termion::terminal_size();
        if let Ok(size) = computed_size {
            width = (size.0 - 1) as usize;
        } else {
            eprintln!("Could not determine terminal size. Defaulting to 80 char width output.");
        }
    }
    width
}


fn main() {
    let matches = setup_clap_interface();
    let config = Config::from_args(&matches);

    if config.mode_is_colortest {
        color_test();
        std::process::exit(0);
    }

    if config.mode_is_demo {
        if let Err(err) = demo(&config) {
            log_error(&err);
            std::process::exit(1);
        }
        std::process::exit(0);
    }

    if let Err(err) = print_heatmap(&config) {
        log_error(&err);
        std::process::exit(1);
    }
}
