document.getElementById('control-icons').addEventListener('click', (ev) => {
  document.getElementById('controls').classList.toggle('clicked');
  document.getElementById('control-icons').classList.toggle('clicked');
  ev.stopPropagation();
});

document.body.addEventListener('click', (ev) => {
  const controls = document.getElementById('controls');
  if (!controls.classList.contains('clicked')) return;
  for (const element of ev.path) {
    if (element === controls) {
      return;
    }
  }
  document.getElementById('controls').classList.remove('clicked');
});

document.getElementById('scheme-dark').addEventListener('change', (ev) => {
  if (ev.target.checked) {
    document.body.className = 'scheme-dark';
  } else {
    document.body.className = 'scheme-light';
  }
});

document.getElementById('scheme-light').addEventListener('change', (ev) => {
  if (ev.target.checked) {
    document.body.className = 'scheme-light';
  } else {
    document.body.className = 'scheme-dark';
  }
});
