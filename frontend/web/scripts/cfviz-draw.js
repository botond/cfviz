// https://github.com/jashkenas/underscore/blob/master/underscore.js
// https://stackoverflow.com/questions/27078285/simple-throttle-in-js/27078401#27078401
// Returns a function, that, when invoked, will only be triggered at most once
// during a given window of time. Normally, the throttled function will run
// as much as it can, without ever going more than once per `wait` duration;
// but if you'd like to disable the execution on the leading edge, pass
// `{leading: false}`. To disable execution on the trailing edge, ditto.
function throttle(func, wait, options, ...args) {
  let timeout;
  let context;
  let argslocal;
  let result;
  let previous = 0;
  let opts;
  if (!options) opts = {};

  function later() {
    previous = opts.leading === false ? 0 : Date.now();
    timeout = null;
    result = func.apply(context, argslocal);
    if (!timeout) context = argslocal = null;
  }

  function throttled() {
    const now = Date.now();
    if (!previous && opts.leading === false) previous = now;
    const remaining = wait - (now - previous);
    context = this;
    argslocal = args;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = func.apply(context, argslocal);
      if (!timeout) context = argslocal = null;
    } else if (!timeout && opts.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  }

  throttled.cancel = function cancel() {
    clearTimeout(timeout);
    previous = 0;
    timeout = context = argslocal = null;
  };

  return throttled;
}

const CodeMetrics = {
  // lineNumbers: null,
  codeBegin: null,
  codeEnd: null,
  lineStarts: [],
  lineEnds: [],
  lineHeight: null,
  lineRects: null,
  scrollTop: null,
  scrollLeft: null,
  DOM: {
    splitMultilineTextNodes() {
      const textNodes = Array.from(document.getElementById('source-code').childNodes).filter(
        // node => node.nodeName === '#text' && /^\n\s+\S/.test(node.textContent)
        node => node.nodeName === '#text' && /^\n\s+/g.test(node.textContent)
      );
      for (const textNode of textNodes) {
        // attempt to isolate newline nodes
        const whitespaceEnd = textNode.textContent.search(/\S/);
        if (whitespaceEnd !== -1) {
          textNode.splitText(textNode.textContent.search(/\S/));
        }
        // 'whitespace' now contains things like 'std', 'branch '
        // i.e. there are two text nodes, one with code, and one with a newline
        // followed by whitespace
        textNode.splitText(1);
        //  now the newline and whitespace are separate nodes
      }
    },
    labelWhitespace() {
      // https://stackoverflow.com/a/25859718
      function wrapTextNode(textNode, element = 'span', className = 'indent') {
        const elementNode = document.createElement(element);
        elementNode.classList.add(className);
        const newTextNode = document.createTextNode(textNode.textContent);
        elementNode.appendChild(newTextNode);
        textNode.parentNode.replaceChild(elementNode, textNode);
      }
      const textNodes = Array.from(document.getElementById('source-code').childNodes).filter(
        node =>
          node.nodeName === '#text' && node.textContent.length > 1 && /^\s+[^\S]/.test(node.textContent)
      );
      const newLineNodes = Array.from(document.getElementById('source-code').childNodes).filter(
        node => node.nodeName === '#text' && node.textContent === '\n'
      );
      for (const node of newLineNodes) {
        if (node.childNodes[0]) {
          console.log(node.childNodes);
          break;
        }
      }
      textNodes.forEach((textNode) => {
        wrapTextNode(textNode, 'span', 'indent');
      });
      newLineNodes.forEach((textNode) => {
        wrapTextNode(textNode, 'span', 'newline');
      });
    },
    wrapLines() {
      const codeNodes = Array.from(document.getElementById('source-code').childNodes);
      if (codeNodes[codeNodes.length - 1].classList.contains('line-numbers-rows')) {
        codeNodes.pop();
      }
      let lineNumber = 0;
      let span = document.createElement('span');
      span.id = `line${lineNumber}`;
      for (const node of codeNodes) {
        if (node.className !== 'newline') {
          span.appendChild(node);
        } else {
          lineNumber++;
          node.parentNode.insertBefore(span, node);
          span = document.createElement('span');
          span.id = `line${lineNumber}`;
        }
      }
    },
    prepare() {
      this.splitMultilineTextNodes();
      this.labelWhitespace();
      this.wrapLines();
    }
  },
  reset() {
    this.codeBegin = null;
    this.codeEnd = null;
    this.lineStarts.length = 0;
    this.lineEnds.length = 0;
    this.lineHeight = null;
    this.lineRects = null;
    this.scrollTop = null;
    this.scrollLeft = null;
  },

  getScrollPosition() {
    this.scrollTop = window.pageYOffset ||
                     document.documentElement.scrollTop ||
                     document.body.scrollTop;
    this.scrollLeft = window.pageXOffset ||
                      document.documentElement.scrollLeft ||
                      document.body.scrollLeft;
    return this;
  },

  getCodeBounds() {
    const bounds = document.getElementById('source-code').getBoundingClientRect();
    this.codeBegin = {
      x: bounds.left + this.scrollLeft,
      y: bounds.top + (this.lineHeight / 2) + this.scrollTop
    };
    // the +20 below is temporary hack to ensure good spacing of descender arrows
    this.codeEnd = {
      x: bounds.right + 20 + this.scrollLeft,
      y: bounds.bottom - (this.lineHeight / 2) + this.scrollTop
    };
    return this;
  },

  getLineHeight() {
    const firstToken = document.getElementsByClassName('token')[0];
    this.lineHeight = firstToken.getBoundingClientRect().height;
    return this;
  },

  // currently assumes that code is *not* of the form:
  // signature()
  // {
  //     // body
  // }
  // i.e. that the only unindented lines are first and last, and that first
  // and last line have no indents
  getLineStarts() {
    this.lineStarts.push(this.codeBegin);
    const indents = document.getElementsByClassName('indent');
    for (const indent of indents) {
      const rect = indent.getBoundingClientRect();
      this.lineStarts.push({
        x: rect.right + this.scrollLeft,
        y: rect.top + (rect.height / 2) + this.scrollTop
      });
    }
    // next line assumes 
    this.lineStarts.push({
      x: this.codeBegin.x,
      y: this.codeEnd.y
    });
    return this;
  },

  getLineEnds() {
    const newlines = document.getElementsByClassName('newline');
    for (const newline of newlines) {
      const rect = newline.getBoundingClientRect();
      this.lineEnds.push({
        x: rect.right + this.scrollLeft,
        y: rect.top + (rect.height / 2) + this.scrollTop
      });
    }
    return this;
  },

  // returns empty promise that resolves when prism.js has finished rendering
  getMetrics() {
    return new Promise((resolve, reject) => {
      // check if Prism.js has already done its work
      if (document.getElementsByClassName('token')[0]) {
        if (!document.getElementsByClassName('indent')[0]) {
          this.DOM.prepare();
        }
        this.getScrollPosition().getLineHeight().getCodeBounds().getLineStarts()
          .getLineEnds();
        return resolve();
      }
      const prismMonitor = new MutationObserver(() => {
        try {
          // mutations.forEach((mutation) => {
          //   const now = new Date();
          //   console.log(
          //     `${mutation.type} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}:${now.getMilliseconds()}`
          //   );
          // });
          if (document.getElementsByClassName('token')[0]) {
            this.DOM.prepare();
            this.getScrollPosition().getLineHeight().getCodeBounds().getLineStarts()
              .getLineEnds();
            prismMonitor.disconnect();
            return resolve();
          }
        } catch (err) {
          return reject(err);
        }
      });
      const target = document.getElementById('source-code');
      const config = { childList: true };
      prismMonitor.observe(target, config);
    });
  },

  getVerticalDistance(line1, line2) {
    return Math.abs(this.lineStarts[line1].y - this.lineStarts[line2].y);
  }
};

// actually will be better to keep count of how many 'reversals' are in the 
// execution array, i.e. in [1,2,3,4,2,1,3], [2,1] is just one reversal, despite
// needing two backward-arrows. Therefore these backward arrows can (and should)
// all be placed in the same "column" aligned left to indicate this. The same
// is true for a sequence of all-forward steps. Probably the array needs to be 
// broken into monotone sequences. 
const Draw = {
  forwardDrawn: 0,
  backwardDrawn: 0,
  reversals: 0,
  arrowSpacing: 20, // px
  arrows: document.createDocumentFragment(),
  // all Draw methods should take only line numbers
  buildJoiningArrows(line1, line2) {
    if (typeof line1 !== 'number' || typeof line2 !== 'number') {
      throw new Error('Invalid input');
    }
    if (line2 === line1) return;
    this.arrows.appendChild(this.buildVerticalPath(line1, line2));
    this.arrows.appendChild(this.buildStartPath(line1, line2));
    this.arrows.appendChild(this.buildEndPath(line1, line2));
    // temporary hack to get spacing between arrows
    if (line1 > line2) {
      this.arrowSpacing += 20;
    }
  },
  buildVerticalPath(line1, line2) { // increments backward drawn
    const path = document.createElement('div');
    const height = CodeMetrics.getVerticalDistance(line1, line2);
    const direction = line1 < line2 ? 'forward' : 'backward';
    const top = CodeMetrics.lineEnds[Math.min(line1, line2)].y;
    const left = line1 < line2
      ? CodeMetrics.codeEnd.x + this.arrowSpacing
      : CodeMetrics.codeBegin.x - this.arrowSpacing;
    path.style.height = `${height}px`;
    path.style.top = `${top}px`;
    path.style.left = `${left}px`;
    path.classList.add('box-arrow', direction, 'path');
    // following is not currently used, but may need to be used to add offset
    // to path.style.left later
    this.backwardDrawn++;
    return path;
  },
  buildStartPath(line1, line2) {
    const path = document.createElement('div');
    const direction = line1 < line2 ? 'forward' : 'backward';
    // this will need to account for number of forward sequences
    // would make most sense to do this at 'width'
    if (line1 < line2) { // i.e. must draw rightwards from line1
      const { x: start, y: top } = CodeMetrics.lineEnds[line1];
      const width = Math.abs(CodeMetrics.codeEnd.x - start) + this.arrowSpacing;
      path.style.left = `${start}px`;
      path.style.width = `${width}px`;
      path.style.top = `${top}px`;
    } else { // i.e. must draw leftwards from line1
      // this will need to account for number of backward sequences
      const { x: start, y: top } = CodeMetrics.lineStarts[line1];
      const width = Math.abs(CodeMetrics.codeBegin.x - start) + this.arrowSpacing;
      path.style.left = `${CodeMetrics.codeBegin.x - this.arrowSpacing}px`;
      path.style.width = `${width}px`;
      path.style.top = `${top}px`;
    }
    path.classList.add('box-arrow', direction, 'path');
    return path;
  },
  buildEndPath(line1, line2) {
    const path = document.createElement('div');
    const direction = line1 < line2 ? 'forward' : 'backward';
    if (line1 < line2) { // i.e. must draw rightwards to codeEnd (+ distance from loops)
      const { x: start, y: top } = CodeMetrics.lineEnds[line2];
      const width = Math.abs(CodeMetrics.codeEnd.x - start) + this.arrowSpacing;
      path.style.left = `${start}px`;
      path.style.width = `${width}px`;
      path.style.top = `${top}px`;
    } else { // i.e. must draw rightwards from codeBegin (+ distance from loops)
      // this will need to account for number of backward sequences
      const { x: start, y: top } = CodeMetrics.lineStarts[line2];
      const width = Math.abs(CodeMetrics.codeBegin.x - start) + this.arrowSpacing;
      path.style.left = `${CodeMetrics.codeBegin.x - this.arrowSpacing}px`;
      path.style.width = `${width}px`;
      path.style.top = `${top}px`;
    }
    path.classList.add('box-arrow', direction, 'path');

    const head = document.createElement('div');
    head.classList.add('box-arrow', direction, 'head');
    path.appendChild(head);
    return path;
  },
  placeArrows() {
    document.body.appendChild(this.arrows);
  },
  colorLine(lineNumber, colorClass = 'unused') {
    const line = document.getElementById(`line${lineNumber}`);
    line.classList.add(colorClass);
    for (const child of line.children) {
      child.classList.add(colorClass);
    }
  }
};

CodeMetrics.getMetrics()
  .then(() => {
    for (let i = 0; i < EXECUTION.length - 1; i++) {
      if (EXECUTION[i + 1] - EXECUTION[i] !== 1) {
        Draw.buildJoiningArrows(EXECUTION[i], EXECUTION[i + 1]);
      }
    }
    Draw.placeArrows();
    window.addEventListener('resize', throttle(() => {
      const arrows = Array.from(document.getElementsByClassName('box-arrow'));
      if (arrows) {
        arrows.forEach((arrow) => { arrow.remove(); });
        Draw.arrowSpacing = 20;
      }
      CodeMetrics.reset();
      // window.scroll(0, 0); // currently a hack to fix buggy behaviour
      CodeMetrics.getMetrics().then(() => {
        for (let i = 0; i < EXECUTION.length - 1; i++) {
          if (EXECUTION[i + 1] - EXECUTION[i] !== 1) {
            Draw.buildJoiningArrows(EXECUTION[i], EXECUTION[i + 1]);
          }
        }
        Draw.placeArrows();
      });
    }, 15));
  })
  .catch((err) => { console.error(err); });

// // currently this is unused

// right now this loops through multiple times quite inefficiently
const ExecutionData = {
  executions: EXECUTION, // located in page html in script tag
  execCounts: [],
  linePairs: (function getLinePairs() {
    const pairs = [];
    for (let i = 0; i < EXECUTION.length - 1; i++) {
      pairs.push({ line1: EXECUTION[i], line2: EXECUTION[i + 1] });
    }
    return pairs;
  }()),
  reversals: [],
  getLineExecutionCounts() {
    // better to get this from the source code in the case of early termination
    const lastLine = Math.max(...EXECUTION);
    this.execCounts.length = lastLine + 1;
    // above lines make assumption that first line (signature) and last line
    // (closing brace) are included in the execution array
    this.execCounts.fill(0);
    EXECUTION.forEach((lineNumber) => {
      console.log(lineNumber);
      this.execCounts[lineNumber]++;
    });
  },
  findReversals() {
    let descentCount = 0;
    let ascentCount = 0;
    let descending = true;
    const execs = this.executions;
    const revs = this.reversals;
    for (let i = 0; i < execs.length - 1; i++) {
      if (execs[i] <= execs[i + 1] && descending) {
        revs[i] = 'none';
      } else if (execs[i] > execs[i + 1] && descending) {
        descending = false;
        revs[i] = 'ascend';
        ascentCount++;
      } else if (execs[i] >= execs[i + 1] && !descending) {
        revs[i] = 'none';
      } else {
        revs[i] = 'descend';
        descending = true;
        descentCount++;
      }
    }
    console.log(`descents: ${descentCount}, ascents: ${ascentCount}`);
    console.log(this.executions);
    console.log(this.reversals);
  },

  prepare() {
  }
};

// currently unused utilities that are helpful to visualize nodes

// function addClientRectsOverlay(elt) {
//   // Absolutely position a div over each client rect so that its border width
//   // is the same as the rectangle's width.
//   // Note: the overlays will be out of place if the user resizes or zooms.
//   const rects = elt.getClientRects();
//   for (let i = 0; i != rects.length; i++) {
//     const rect = rects[i];
//     const tableRectDiv = document.createElement('div');
//     tableRectDiv.style.position = 'absolute';
//     tableRectDiv.style.border = '1px solid red';
//     const scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
//     const scrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
//     tableRectDiv.style.margin = tableRectDiv.style.padding = '0';
//     tableRectDiv.style.top = `${rect.top + scrollTop}px`;
//     tableRectDiv.style.left = `${rect.left + scrollLeft}px`;
//     // we want rect.width to be the border width, so content width is 2px less.
//     tableRectDiv.style.width = `${rect.width - 2}px`;
//     tableRectDiv.style.height = `${rect.height - 2}px`;
//     document.body.appendChild(tableRectDiv);
//   }
// }

// document.addEventListener('click', (e) => {
//   if (e.metaKey) {
//     console.log(`\nclient X,Y = [${e.clientX}, ${e.clientY}]`);
//     console.log(`CodeMetrics.codeEnd = [${CodeMetrics.codeEnd.x},${CodeMetrics.codeEnd.y}]`);
//     console.log(`CodeMetrics.codeBegin = [${CodeMetrics.codeBegin.x},${CodeMetrics.codeBegin.y}]`);
//   }
// });

// // from https://jsperf.com/measure-text-width/4 and
// // https://stackoverflow.com/questions/16209153/ how-to-get-the-position-and-size-of-a-html-text-node-using-javascript
// function measureTextWidth(text) {
//   const canvas = document.createElement('canvas');
//   const docFragment = document.createDocumentFragment();
//   docFragment.appendChild(canvas);
//   return canvas.getContext('2d').measureText(text).width;
// }

// function outlineTextNodes(textNodes) {
//   for (const textNode of textNodes) {
//     const range = document.createRange();
//     range.selectNodeContents(textNode);
//     addClientRectsOverlay(range);
//   }
// }

// // https://stackoverflow.com/a/16210994/8232219
// function getTextNodeRects(textNode) {
//   const range = document.createRange();
//   range.selectNodeContents(textNode);
//   const rects = range.getClientRects();
//   addClientRectsOverlay(range);
//   return rects;
// }

// drawDot(x, y) {
//   const dot = document.createElement('div');
//   dot.classList.add('dot');
//   [dot.style.left, dot.style.top] = [`${x}px`, `${y}px`];
//   document.body.appendChild(dot);
//   return dot;
// }
