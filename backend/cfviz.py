from __future__ import print_function
from contextlib import contextmanager

import gdb
import json
import os
import tempfile

def contains(block, address):
    # It looks like Block.end returns a past-the-end address?
    return address >= block.start and address < block.end

class LineRange:
    def __init__(self):
        self.start = None
        self.end = None

    def add(self, line):
        if self.start is None:
            self.start = line
            self.end = line
        elif self.start > line:
            self.start = line
        elif self.end < line:
            self.end = line

# Check whether we are running rr (true) or vanilla gdb (false)
def is_rr():
    # Try instantiating the type "RRCmd".
    # In vanilla gdb, this will fail with a NameError.
    # In rr, this will succeed.
    try:
        # To succeed without exceptions, the RRCmd constructor
        # requires two arguments, the first of which is nonempty.
        waldo = RRCmd("waldo", "")
    except NameError as e:
        return False
    return True

# A context manager (see https://docs.python.org/2/library/contextlib.html)
# for running some code with breakpoints disabled.
# Usage:
#   with breakpoints_disabled():
#       # code to run while breakpoints are disabled
#   # breakpoint enabled/disabled state will be restored at the end
#   # of the "with" block
@contextmanager
def breakpoints_disabled():
    # Save the enabled/disable state of each breakpoint,
    # so that it can be restored.
    print("Disabling breakpoints")
    breakpoint_enabled_states = []
    for breakpoint in gdb.breakpoints():
        breakpoint_enabled_states.append(breakpoint.enabled)
        breakpoint.enabled = False

    # Use a try...finally block to ensure breakpoints are restored even
    # if an exception is thrown by the code that runs during the "yield".
    try:
        # Run the code that needs to be run with breakpoints disabled.
        yield
    finally:
        # Restore the breakpoints.
        print("Restoring breakpoints")
        for (breakpoint, enabled_state) in zip(gdb.breakpoints(),
                                           breakpoint_enabled_states):
            breakpoint.enabled = enabled_state

# Define the "cfviz" command
class CFViz(gdb.Command):
    def __init__(self):
        super (CFViz, self).__init__("cfviz", gdb.COMMAND_SUPPORT,
                                     gdb.COMPLETE_NONE, True)
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.cfviz_dir = os.path.join(script_dir, "..")
        print("cfviz_dir = ", self.cfviz_dir)

    def invoke(self, arg, from_tty):
        # On the first invocation of the command, detect whether we are
        # running in rr or vanilla gdb.
        # Note that this cannot be done in __init__, because __init__ may
        # be called before the rr plugin is loaded.
        if not hasattr(self, 'is_rr'):
            self.is_rr = is_rr()
            if self.is_rr:
                print("This is rr")
            else:
                print("This is vanilla gdb")

        # Disable all breakpoints while performing navigation actions,
        # to make sure they are not interrupted by a breakpoint.
        with breakpoints_disabled():
            # If we're running in rr, go to the beginning of the function.
            # In vanilla gdb, the user will just get a visualization of
            # the remainder of the function's execution.
            if self.is_rr:
                gdb.execute("reverse-finish", to_string=True)

                # Step into function.
                # This is needed since "reverse-finish" takes you to just
                # before the beginning of the function.
                gdb.execute("s", to_string=True)

            # Gather basic information about function
            target_frame = gdb.selected_frame()
            target_symtab = target_frame.find_sal().symtab
            filename = target_symtab.fullname()
            print("Chose function", target_frame.name(), "in file", filename)

            # Determine the function's extents in the source file
            line_range = LineRange()
            target_block = target_frame.block()
            #print("Block extents are [", target_block.start, ",", target_block.end, ")")
            for line in target_symtab.linetable():
                #print("Considering line ", (line.line, line.pc))
                if contains(target_block, line.pc):
                    line_range.add(line.line)
            #print("Line range is [", line_range.start, ",", line_range.end, "]")

            # Step through the function and record the sequence of
            # executed lines.
            executed_lines = []
            while gdb.selected_frame() == target_frame:
                sal = gdb.selected_frame().find_sal()
                executed_lines.append(sal.line)
                gdb.execute("n", to_string=True)
            #print("Executed lines:", executed_lines)

        # Get the function's source code based on the line range
        file = open(filename, "r")
        all_lines = file.readlines()
        # LineRange's line numbers are 1-based and inclusive.
        # Python slices take a 0-based half-open range.
        function_lines = all_lines[line_range.start-1:line_range.end]
        function_src = "".join(function_lines)
        #print("Function's source code is:", function_src)

        # Adjust executed_lines to be based on the beginning of
        # the function's line range, as opposed to the whole file
        executed_lines = [line - line_range.start for line in executed_lines]
        #print("Adjusted executed lines:", executed_lines)

        # Construct the json that the frontend expects
        json_data = json.dumps({
                "function": function_src,
                "execution": executed_lines,
                "first_line_number": line_range.start
            },
            indent = 4,
            separators = (',',  ': '))
        #print("JSON data:", json_data)

        # Write the data to a temporary file
        json_file = tempfile.NamedTemporaryFile(delete = False)
        json_file.write(json_data.encode('UTF-8'))
        json_file.close()
        print("Wrote data to file", json_file.name)

        # The backend does not currently have arguments of its own,
        # so all arguments passed to the backend are passed through
        # to the frontend.
        frontend_args = arg

        # Invoke the frontend with the temp file as input
        cmd = "cd " + self.cfviz_dir + \
              " && frontend/cfviz-frontend " + frontend_args + " " + json_file.name
        print("Executing command:", cmd)
        gdb.execute("shell " + cmd)

# Register the command
CFViz()
